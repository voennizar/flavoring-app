package com.voen.flavoringapp

import android.app.Application
import com.voen.flavoringapp.module.viewModelModule
import org.koin.core.context.startKoin

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(viewModelModule)
        }
    }
}