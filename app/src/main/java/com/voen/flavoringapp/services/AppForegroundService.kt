package com.voen.flavoringapp.services

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.voen.flavoringapp.SecondActivity

class AppForegroundService : Service() {
    companion object {
        const val ACTION_START = "action_start"
        const val ACTION_CLOSE = "action_close"
        private const val CHANNEL_ID = "notif_channel"
        private const val CHANNEL_TITLE = "title_channel"
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            validateAction(it.action)
        }

        return super.onStartCommand(intent, flags, startId)
    }

    private fun validateAction(action: String?) {
        when (action) {
            ACTION_START -> {
                val intent = Intent(this, SecondActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
                val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val notification: NotificationCompat.Builder?
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val channel = NotificationChannel(
                        CHANNEL_ID,
                        CHANNEL_TITLE,
                        NotificationManager.IMPORTANCE_NONE
                    )
                    notificationManager.createNotificationChannel(channel)
                }

                notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(android.R.drawable.ic_media_play)
                    .setContentTitle("Playing Music")
                    .setWhen(System.currentTimeMillis())
                    .setFullScreenIntent(pendingIntent, true)


                startForeground(1, notification.build())
            }
            ACTION_CLOSE -> {
                stopForeground(true)
            }
        }
    }
}