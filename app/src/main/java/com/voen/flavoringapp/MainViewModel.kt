package com.voen.flavoringapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.voen.flavoringapp.data.AppRepository


class MainViewModel(private val repository: AppRepository) : ViewModel() {
    private val newsData = MutableLiveData<String>()

    fun observeNewsData(): LiveData<String> = newsData

    fun fetchNews() {
        val result = repository.fetchNews()
        newsData.postValue(result)
    }
}