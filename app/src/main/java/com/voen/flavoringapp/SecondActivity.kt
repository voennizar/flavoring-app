package com.voen.flavoringapp

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import org.koin.android.scope.currentScope
import org.koin.android.viewmodel.ext.android.viewModel

class SecondActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "voenxyz"
    }

    private val mainViewModel: MainViewModel by viewModel()
    private val utils: Utils by currentScope.inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        mainViewModel.fetchNews()
        mainViewModel.observeNewsData().observe(this, Observer {
            Log.d(TAG, "Result Second : $it")
        })

        multiply(3, 3)
        Log.d(TAG, "Plus Second : ${utils.plus(3, 3)}")
    }
}