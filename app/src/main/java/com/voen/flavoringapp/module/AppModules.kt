package com.voen.flavoringapp.module

import com.voen.flavoringapp.MainViewModel
import com.voen.flavoringapp.Utils
import com.voen.flavoringapp.data.AppRepository
import com.voen.flavoringapp.network.ApiService
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    single { ApiService() }
    single { AppRepository(get()) }

    viewModel { MainViewModel(get()) }

    // uncomment this to use scoped injection
//    scope(named<MainActivity>()) {
//        scoped {
//            Utils()
//        }
//    }
    single { Utils() }
}