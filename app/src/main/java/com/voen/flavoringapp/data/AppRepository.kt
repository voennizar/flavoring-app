package com.voen.flavoringapp.data

import com.voen.flavoringapp.network.ApiService

class AppRepository(private val apiService: ApiService) {
    fun fetchNews() = apiService.fetchNews()
}