package com.voen.flavoringapp

// tightly coupled
class Car(engine: Engine) {
    private var engine: Engine? = engine

    fun start() {
        engine?.start()
    }
}

class Boat() {
    var engine: Engine? = null

    fun start() {
        engine?.start()
    }
}

class Engine(type: String) {
    private var type: String? = type

    fun start() {

    }
}

fun main() {
    val engine = Engine("gas")
    val gasCar = Car(engine)
    gasCar.start()

    val electricEngine = Engine("electric")
    val electricCar = Car(electricEngine)
    electricCar.start()

    val boatEngine = Engine("gas")
    val boat = Boat()
    boat.engine = boatEngine
    boat.start()
}