package com.voen.flavoringapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.voen.flavoringapp.services.AppForegroundService
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.scope.currentScope
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "voenxyz"
    }

    private val mainViewModel: MainViewModel by viewModel()
    private val utils: Utils by currentScope.inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tv_hello.setOnClickListener {
            tv_hello.performLongClick()
        }
        bt_start.setOnClickListener {
            mainViewModel.fetchNews()
            Log.d(TAG, "Plus : ${utils.plus(2, 2)}")
        }

        bt_stop.setOnClickListener {
            stopForegroundService()
            startActivity(Intent(this, SecondActivity::class.java))
        }

        mainViewModel.observeNewsData().observe(this, Observer {
            Log.d(TAG, "Result : $it")
        })
    }

    override fun onPause() {
        super.onPause()

        startForegroundService()
    }

    private fun startForegroundService() {
        val intent = Intent(this, AppForegroundService::class.java)
        intent.action = AppForegroundService.ACTION_START

        startService(intent)
    }

    private fun stopForegroundService() {
        val intent = Intent(this, AppForegroundService::class.java)
        intent.action = AppForegroundService.ACTION_CLOSE

        startService(intent)
    }
}
